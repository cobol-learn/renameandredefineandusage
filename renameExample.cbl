       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REDEFINE-EXAMPLE2.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STU-REC.
           05 STUDENT-ID  PIC   9(8) VALUE 12345678.
           05 GPA  PIC   9V99 VALUE 3.25.
           05 FORE-NAME   PIC   X(6) VALUE "Matt".
           05 SUR-NAME   PIC   X(8) VALUE "Cullen".
           05 GENDER   PIC   X VALUE "M".
           05 PHONE   PIC   X(14) VALUE "3536120228233".
      *RENAMES
       66  PERSONAL-INFO  RENAMES FORE-NAME THROUGH PHONE .
       66  COLLEGE-INFO  RENAMES STUDENT-ID  THROUGH SUR-NAME  .
       66  STUDENT-NAME  RENAMES FORE-NAME   THROUGH SUR-NAME   .
      *=============================================================
       01  CONTACT-INFO.
           05 STU-NAME.
              10 STU-FORENAME   PIC   X(6).
              10 STU-SURNAME PIC   X(8).
           05 STU-GENDER  PIC X.
           05 STU-PHONE   PIC   X(14).
      *REANMES
       66  MY-PHONE RENAMES STU-PHONE .




       PROCEDURE DIVISION .
           DISPLAY "EXAMPLE1 "
           DISPLAY "ALL INFORMATION = " STU-REC 
           DISPLAY "COLLEGE-INFO = " COLLEGE-INFO 
           DISPLAY "PERSONAL  INFO = " PERSONAL-INFO 
      *    ------------------------------------------
           DISPLAY "EXAMPLE2"
           DISPLAY "COMBINED NAMES = " STUDENT-NAME 
      *    -----------------------------------------
           MOVE  PERSONAL-INFO TO  CONTACT-INFO 
           DISPLAY "EXAMPLE3"
           DISPLAY "NAME IS " STU-NAME  
           DISPLAY "GENDER IS " STU-GENDER  
           DISPLAY "PHONE IS " STU-PHONE  
      *    -----------------------------------------
           DISPLAY "EXAMPLE4"
           DISPLAY "MYPHONE IS "  MY-PHONE 
           GOBACK 
       .