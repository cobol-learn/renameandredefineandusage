       IDENTIFICATION DIVISION. 
       PROGRAM-ID. USAGE-COMP.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  A  PIC   S9(8) USAGE DISPLAY .
       01  AC  PIC   S9(8) USAGE COMPUTATIONAL  .
       01  GABC  USAGE COMP.
           05 GA PIC   9(3). 
           05 GB PIC   9(4)V99 USAGE DISPLAY.
           05 GC PIC   S9(5) USAGE COMP SYNC.
           05 GD PIC   S9(5).
      

       PROCEDURE DIVISION .
           INITIALIZE A
           MOVE '1234567' TO A.
           MOVE '1234567' TO AC.
      *    ==============================
      *    +01234567
           DISPLAY A
      *    8
           DISPLAY 'LENGHT ' LENGTH OF A
      *    ==============================
      *    +01234567
           DISPLAY AC
      *    4 เพราะอยู่ในช่วง 9(5-9) => 4byte  
           DISPLAY 'LENGHT ' LENGTH OF AC
      *    ==============================
      *    2 : (1-4)
           DISPLAY 'LENGHT GA ' LENGTH OF GA 
      *    6 => usage is display
           DISPLAY 'LENGHT GB ' LENGTH OF GB 
      *    4 : (5-9)
           DISPLAY 'LENGHT GC ' LENGTH OF GC
      *    4 : (5-9)
           DISPLAY 'LENGHT GD ' LENGTH OF GD

           GOBACK 
       .