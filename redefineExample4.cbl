       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REDEFINE-EXAMPLE2.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  OILS-TABLE.
           05 OIL-TABLE-VALUE.
              10 FILLER PIC X(28) VALUE "Almond 020003500650".
              10 FILLER PIC X(28) VALUE "Aloe vera 047508501625".
              10 FILLER PIC X(28) VALUE "Apricot kernel 025004250775".
              10 FILLER PIC X(28) VALUE "Avocado 027504750875".
              10 FILLER PIC X(28) VALUE "Coconut 027504750895".
              10 FILLER PIC X(28) VALUE "Evening primrose037506551225".
              10 FILLER PIC X(28) VALUE "Grape seed 018503250600".
              10 FILLER PIC X(28) VALUE "Peanut 027504250795".
              10 FILLER PIC X(28) VALUE "Jojoba 072513252500".
              10 FILLER PIC X(28) VALUE "Macadamia 032505751095".
              10 FILLER PIC X(28) VALUE "Rosehip 052509951850".
              10 FILLER PIC X(28) VALUE "Sesame 029504250750".
              10 FILLER PIC X(28) VALUE "Walnut 027504550825".
              10 FILLER PIC X(28) VALUE "Wheatgerm 045007751425".
      *level จะต้องเป็น level เดียวกัน และเป็น 66 , 88 ไม่ได้
           05 FILLER   REDEFINES OIL-TABLE-VALUE .
              10 BASE-OIL OCCURS 14 TIMES.
                 15 OIL-NAME PIC   X(16).
                 15 UNIT-COST   PIC   99V99 OCCURS 3 TIMES.
       01  IDX PIC 99.

       PROCEDURE DIVISION .
           PERFORM VARYING IDX FROM 1 BY 1 UNTIL IDX > 14
            DISPLAY BASE-OIL(IDX)
           END-PERFORM
           
           GOBACK 
       .